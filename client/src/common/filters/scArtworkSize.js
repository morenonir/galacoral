/**
 * Created by nirmoreno on 5/24/15.
 */

(function () {
  'use strict';

  function filter() {
    return function (url, size) {

      if (!angular.isString(url) || !url.length) return url;

      switch (size) {
        case 300:
          url = url.replace('large.jpg', 't300x300.jpg');
          break;
        case 500:
          url = url.replace('large.jpg', 't500x500.jpg');
          break;
        default:
      }
      return url;
    };
  }

  angular.module('common.filter.scArtworkSize', [])
    .filter('scArtworkSize', filter);
})();
