;
(function () {
  "use strict";


  /**
   * filter
   */

  var patterns = ["-", "(", ":", "..", "#", "[", "~"];

  function filter() {
    return function (fullTitle) {

      if (fullTitle === "") return "No title";

      var patternIndex;
      for (var i in patterns) {
        patternIndex = fullTitle.indexOf(patterns[i]);
        if (patternIndex > 2) {
          fullTitle = fullTitle.substr(0, patternIndex);
        } else if (patternIndex >= 0 && patternIndex < 2) {
          return fullTitle;
        }
      }

      return fullTitle;
    };
  }

  /**
   * angular module
   */

  angular.module("common.filters.shortTitle", [])
    .filter("shortTitle", filter)

})();
