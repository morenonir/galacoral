/**
 * Created by nirmoreno on 5/22/15.
 */

;
(function () {
  'use strict';

  var _tag = "[Sound Cloud Service]",
    _sc_clientKey = "d652006c469530a4a7d6184b18e16c81";

  /**
   * Service
   */

  function service($q) {

    var sc = this;


    sc.init = init;
    sc.getTracksByName = getTracksByName;

    /************************************************
     * Functions
     */

    function init() {
      SC.initialize({
        client_id: _sc_clientKey
      });
    }

    function getTracksByName(name) {

      var deferred = $q.defer();

      if (!angular.isString(name) || !name.length) deferred.reject("Please enter a valid string");

      SC.get('/tracks', {q: name}, deferred.resolve);

      return deferred.promise;

    }

  }


  /**
   * Run
   */

  function run(SoundCloud) {

    SoundCloud.init();

  }

  /**
   * Angular module
   */

  angular.module('common.services.soundCloud', [])
    .service('SoundCloud', service)
    .run(run);
})();
