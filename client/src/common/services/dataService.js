(function() {
  'use strict';

  function dataService() {
    var ds = this;
    ds.trackObject = undefined;
  }

  angular.module('common.services.data', [])
    .service('DataService', dataService);
})();
