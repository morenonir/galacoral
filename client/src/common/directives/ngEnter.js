/**
 * Created by nirmoreno on 4/09/15.
 */

(function () {
  'use strict';

  function directive() {

    return function (scope, element, attrs) {
      element.bind("keydown keypress", function (event) {
        if(event.which === 13) {
          scope.$apply(function (){
            scope.$eval(attrs.ngEnter);
          });
          event.preventDefault();
        }
      });
    };
  }

  angular.module('common.directives.ngEnter', [])
    .directive('ngEnter', directive);
})();
