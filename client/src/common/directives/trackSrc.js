/**
 * Created by nirmoreno on 5/24/15.
 */

(function () {
    'use strict';

  function directive() {

    return {
      restrict: 'A',

      link: function (scope, element, attrs) {
        var current = element;
        scope.$watch(function () {
          return attrs.trackSrc;
        },function () {
          var url = 'http://player.soundcloud.com/player.swf?url=' + attrs.trackSrc + '&enable_api=true&show_comments=false&color=000000&auto_play=true&object_id=myPlayerId';
          var clone = element
            .clone()
            .attr('src', url);
          current.replaceWith(clone);
          current = clone;
        });
      }
    };


  }


  angular.module('common.directives.embedSrc', [])
        .directive('trackSrc', directive);
})();
