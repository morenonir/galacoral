;
(function () {
  'use strict';

  function directive() {

    return {
      restrict: 'E',
      link: function (scope, elm, attrs) {

        elm.on("error", function (e) {
          elm.attr("src","/assets/images/image-err.png");
        });

        if (!attrs.src){
          elm.attr("src","/assets/images/image-err.png");
        }

      }
    };
  }


  angular.module('common.directive.imgErr', [])
    .directive('img', directive);
})();
