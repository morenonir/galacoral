(function () {
  'use strict';

  angular.element(document).ready(function () {
    angular.bootstrap(document, ['app']);
  });

  function config($stateProvider, $urlRouterProvider, $logProvider, $httpProvider, localStorageServiceProvider) {
    $urlRouterProvider.otherwise('/');
    $logProvider.debugEnabled(true);
    $httpProvider.interceptors.push('httpInterceptor');
    $stateProvider
      .state('root', {
        views: {
          'header': {
            templateUrl: 'src/common/header.tpl.html',
            controller: 'HeaderCtrl'
          },
          'footer': {
            templateUrl: 'src/common/footer.tpl.html',
            controller: 'FooterCtrl'
          }
        }
      });
    localStorageServiceProvider
      .setPrefix('galaCoral');
  }

  function MainCtrl($log, $scope) {
    $log.debug('MainCtrl laoded!');
  }

  function run($log) {
    $log.debug('App is running!');
  }

  angular.module('app', [
    'ui.bootstrap',
    'ui.router',
    'home',
    'track',
    'common.header',
    'common.footer',
    'common.services.soundCloud',
    'common.services.data',
    'common.directives.version',
    'common.directive.imgErr',
    'common.directives.embedSrc',
    'common.directives.ngEnter',
    'common.filters.uppercase',
    'common.filters.shortTitle',
    'common.filter.scArtworkSize',
    'common.interceptors.http',
    'templates',
    'ngMaterial',
    'angularUtils.directives.dirPagination',
    'LocalStorageModule',
    'ngMdIcons',
    'ngAnimate',
    'anim-in-out'
  ])
    .config(config)
    .run(run)
    .controller('MainCtrl', MainCtrl)
    .value('version', '1.1.0');
})();
