(function () {
  'use strict';

  var _tag = "[Home ctrl]";

  /**
   * @name  config
   * @description config block
   */
  function config($stateProvider) {
    $stateProvider
      .state('root.home', {
        url: '/home',
        views: {
          '@': {
            templateUrl: 'src/app/home/home.tpl.html',
            controller: 'HomeCtrl'
          }
        }
      });
  }

  /**
   * @name  HomeCtrl
   * @description Controller
   */
  function ctrl($log, $scope, $state, SoundCloud, localStorageService, DataService) {

    $scope.searchQueries = localStorageService.get('searchQueries') || [];

    $scope.viewModes = {
      list: 0,
      tile: 1
    };
    $scope.currentViewMode = localStorageService.get("currentViewMode") || $scope.viewModes.list;
    $scope.isLoader = false;
    $scope.tracks = [];
    $scope.searchInput = "coldplay";
    $scope.chunkSize = 6;
    $scope.isRecentSearchClosed = true;
    $scope.searchByText = searchByText;
    $scope.openTrack = openTrack;
    $scope.setCurrentViewMode = setCurrentViewMode;


    searchByText();

    /****************************************
     * Functions
     */

    function setCurrentViewMode(mode){
      localStorageService.set("currentViewMode", mode);
      $scope.currentViewMode = mode;

    }

    function searchByText(){
      $scope.isLoader = true;
      addQueryToLocalStorage($scope.searchInput);
      SoundCloud
        .getTracksByName($scope.searchInput)
        .then(function (tracks) {
          //$log.debug(tracks);
          $scope.tracks = tracks;
          $scope.isLoader = false;
        }, function (err) {
          $scope.isLoader = false;
          //$log.debug(_tag, "[ERROR] can't reach data from sound cloud. err:", err);
        });
    }

    function addQueryToLocalStorage(query){
      //if($scope.searchQueries.length === 5 ){
      //  $scope.searchQueries = $scope.searchQueries.splice(1,$scope.searchQueries.length-1);
      //}
      $scope.searchQueries.unshift(query);
      localStorageService.set("searchQueries",$scope.searchQueries);
      //$log.debug(_tag,localStorageService.get("searchQueries"));
     }

    function openTrack(track){
      DataService.trackObject = track;
      $state.go('root.track');
    }
  }

  angular.module('home', [])
    .config(config)
    .controller('HomeCtrl', ctrl);
})();
