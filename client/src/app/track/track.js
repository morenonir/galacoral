/**
 * Created by nirmoreno on 5/23/15.
 */
(function () {
  'use strict';

  var _tag = "[Track ctrl]";

  /**
   * @name  config
   * @description config block
   */
  function config($stateProvider) {
    $stateProvider
      .state('root.track', {
        url: '/track',
        views: {
          '@': {
            templateUrl: 'src/app/track/track.tpl.html',
            controller: 'TrackCtrl'
          }
        },
        resolve: {
          PageData: ["DataService", "$state",
            function (DataService, $state) {
              if (!angular.isObject(DataService.trackObject)) {
                return false;
              }

              return DataService.trackObject;
            }]
        }
      });
  }

  /**
   * @name  HomeCtrl
   * @description Controller
   */
  function ctrl($log, $state, $scope, PageData, SoundCloud) {
    $log.debug(_tag, "[Load successfully]");

    $scope.myPlayerId = 'myPlayerId';
    $scope.myPlayerName = 'playerName';

    if (PageData === false) {
      goToMain();
      return;
    }

    $scope.track = PageData;
    //$scope.imageUrl = $scope.track.artwork_url.replace('large.jpg', 't500x500.jpg');

    $scope.goToMain = goToMain;
    $scope.playTrigger = playTrigger;

    function goToMain() {
      $state.transitionTo('root.home');
      return;
    }

    function playTrigger() {
      soundcloud.getPlayer("myPlayerName").api_play();
    }


    $scope.$on("$destroy", function () {
      soundcloud.getPlayer("myPlayerName").api_pause();
    });


  }

  angular.module('track', [])
    .config(config)
    .controller('TrackCtrl', ctrl);
})();
